$('.form__select,.field__select').select2({
  minimumResultsForSearch: -1,
  language: {
    noResults: function (params) {
      return "Не найдено";
    }
  },
  searching: function () {
    return 'Поиск…';
  },
});

$('.form__select--search').select2({
  language: {
    noResults: function (params) {
      return "Не найдено";
    }
  },
  searching: function () {
    return 'Поиск…';
  },
//  minimumResultsForSearch: -1,
});



$('.form__select,.field__select').on("select2:open", function () {
  $('.select2-results__options').perfectScrollbar();
    setTimeout(function () {
      $('.select2-results__options').perfectScrollbar('update');
    }, 1);
});