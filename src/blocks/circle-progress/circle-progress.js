function progressInit(animation){
  $('.circle-progress__bar').each(function(i,el){
    $(el).circleProgress({
      startAngle: -Math.PI/2,
      size: $(el).outerWidth(),
      fill: '#'+$(el).data('fill-color'),
      thickness: 10,
      animation: animation,
      emptyFill: '#D0D0D0'
    });
  });
}
progressInit({duration: 1200, easing: "circleProgressEasing"});
$(window).resize(function(){
  progressInit(false);
});