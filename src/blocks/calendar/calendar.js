if ($(".calendar")[0]){
  $(function() {
    $('.calendar').datepicker({
      yearRange: "-100:+3",
      range: 'period',
      defaultDate: null,
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd.mm.yy",
      numberOfMonths: 1,
      onSelect: function(dateText, inst, extensionRange) {
        if ($('.ui-datepicker-calendar .selected-start.selected-end')[0]) {
          var dates = extensionRange.startDateText + ' - ' + extensionRange.endDateText;
          $('.calendar').val(dates);
          if ($(".sorting__select.calendar")[0]) {
            $(".sorting__select.calendar").css('width', dates.length * 7.2 + 'px');
          }
        }
        else {
          var dates = extensionRange.startDateText;
          $('.calendar').val(dates)
          if ($(".sorting__select.calendar")[0]) {
            $(".sorting__select.calendar").css('width', dates.length * 8.4 + 'px');
          }
        }
      },
    });
  });
}



if ($(".calendar--single-date")[0]){
  $(function() {
    $('.calendar--single-date').datepicker({
      yearRange: "-100:+3",
      defaultDate: null,
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd.mm.yy",
      numberOfMonths: 1,
    });
  });
}



if ($(".calendar--month-year")[0]){
  $(function() {
    $('.calendar--month-year').datepicker({
      yearRange: "-100:+3",
      defaultDate: null,
      changeMonth: true,
      changeYear: true,
      dateFormat: 'MM yy',
      onClose: function(dateText, inst) { 
          var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
          var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
          $(this).datepicker('setDate', new Date(year, month, 1));
      },
      beforeShow : function(input, inst) {
          var datestr;
          if ((datestr = $(this).val()).length > 0) {
              year = datestr.substring(datestr.length-4, datestr.length);
              month = jQuery.inArray(datestr.substring(0, datestr.length-5), $(this).datepicker('option', 'monthNamesShort'));
              $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
              $(this).datepicker('setDate', new Date(year, month, 1));
          }
      }
    }).focus(function () {
      $(".ui-datepicker-calendar").hide();
    });
  });
}