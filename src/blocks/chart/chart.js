var ctx = document.getElementById('chart').getContext('2d');
var gradient = ctx.createLinearGradient(0, 0, 0, 360);
var gradient2 = ctx.createLinearGradient(0, 0, 0, 360);
gradient.addColorStop(0, '#1F85C3');
gradient.addColorStop(1, 'rgba(255, 255, 255, 0)');
gradient2.addColorStop(0, '#F4A601');
gradient2.addColorStop(1, 'rgba(244, 166, 1, 0)');
var chart = new Chart(ctx, {
  // The type of chart we want to create
  type: 'line',

  // The data for our dataset
  data: {
    labels: ['1','2','3','4','5','6','7','8'],
    datasets: [{
      label: 'Показатель один',
      backgroundColor: gradient,
      borderColor: '#1F85C3',
      data: [0, 10, 5, 2, 20, 30, 45 ,30]
    },{
      label: 'Показатель два',
      backgroundColor: gradient2,
      borderColor: '#FF8A00',
      data: [1, 2, 15, 25, 20, 31, 38 ,45]
    }]
  },

  // Configuration options go here
  options: {
    responsive: true,
    legend: {
      position: 'bottom'
    }
  }
});