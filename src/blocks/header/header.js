$('.header__user-button').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('header__user-button--active');
  $('.header__user-content').fadeToggle('fast');
});