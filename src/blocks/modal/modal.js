$('[data-fancybox]').fancybox({
  touch: false,
  autoFocus: false,
});

$('.modal__close').click(function(e) {
  e.preventDefault();
  parent.$.fancybox.close();
});


/* if здесь для примера, условие может быть любым */

if ($('.body--login').length) {
  $(".header__user").trigger('click');
};
