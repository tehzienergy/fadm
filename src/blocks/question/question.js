$('.question__header').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('question__header--opened');
  $(this).next('.question__content').slideToggle('fast');
});