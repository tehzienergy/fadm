$('.slider').slick({
  slidesToShow: 1,
  centerMode: true,
  centerPadding: '146px',
  dots: true,
  responsive: [
    {
      breakpoint: 1025,
      settings: {
        centerPadding: '50px',
      }
    },
    {
      breakpoint: 576,
      settings: {
        arrows: false,
        centerMode: false
      }
    }
  ]
});