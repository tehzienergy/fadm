$('.sorting__select--common').select2({
  minimumResultsForSearch: -1,
  dropdownAutoWidth: true,
  dropdownCssClass: "sorting__dropdown",
  language: {
    noResults: function (params) {
      return "Не найдено";
    }
  },
  searching: function () {
    return 'Поиск…';
  },
});

$('.sorting__select').on("select2:open", function () {
  $('.select2-results__options').perfectScrollbar();
    setTimeout(function () {
      $('.select2-results__options').perfectScrollbar('update');
    }, 1);
});




$('.sorting__select--multiple').select2({
  minimumResultsForSearch: -1,
  placeholder: "Все регионы",
  dropdownAutoWidth: true,
  containerCssClass: 'select2--region',
  closeOnSelect: false,
  language: {
    noResults: function (params) {
      return "Не найдено";
    }
  },
  searching: function () {
    return 'Поиск…';
  },
  templateResult: function(option) {
    var $option = $(
      '<div class="select2-option--checkbox">' + option.text + '</div>'
    );
    return $option;
  }
});


if ($('.sorting__select--multiple + .select2 .select2-selection__rendered .select2-selection__choice').length > 1) {
  var uldiv = $('.sorting__select--multiple').siblings('span.select2').find('ul')
  var count = uldiv.find('li').length - 2;
  uldiv.html("<li>Города: "+count+"</li>")
}

$('.sorting__select--multiple').on('select2:select', function (evt) {
  if ($('.select2-selection__rendered .select2-selection__choice').length > 1) {
	var uldiv = $(this).siblings('span.select2').find('ul')
    var count = uldiv.find('li').length - 1;
 	uldiv.html("<li>Города: "+count+"</li>")
  }
});

$('.sorting__select--multiple').on('select2:unselect', function (evt) {
  if ($('.select2-selection__rendered .select2-selection__choice').length > 1) {
	var uldiv = $(this).siblings('span.select2').find('ul')
    var count = uldiv.find('li').length - 1;
 	uldiv.html("<li>Города: "+count+"</li>")
  }
});

$('.sorting__select--multiple').change(function() {
    if ($(this).children('option[value=allCities]').is(':selected')) {
      $(this).val('').change();
      $('.sorting__select--multiple').select2("close");
    }
});

  
//if ($(".sorting__select--multiple")[0]){
//  $(function() {
//    $(".sorting__select--multiple .select2-search__field").css('width', $(this).attr('placeholder').length + 10);
//  });
//}