$('.search__button').click(function(e) {
  e.preventDefault();
  $('.search').addClass('search--active');
  $('body').addClass('body--fixed');
  $('.search__input').focus();
});

$('.search__close').click(function(e) {
  e.preventDefault();
  $('.search').removeClass('search--active');
  $('body').removeClass('body--fixed');
});
