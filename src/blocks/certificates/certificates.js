$('.certificates').slick({
  infinite: false,
  variableWidth: true,
  nextArrow: '<button type="button" class="certificates__arrow certificates__arrow--next"></button>',
  prevArrow: '<button type="button" class="certificates__arrow certificates__arrow--prev"></button>'
});