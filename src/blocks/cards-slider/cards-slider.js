$('.cards-slider').on('setPosition', function () {
  $(this).find('.slick-slide').height('auto');
  var slickTrack = $(this).find('.slick-track');
  var slickTrackHeight = $(slickTrack).height();
  $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
});

$('.cards-slider').slick({
//  autoplay: true,
  autoplaySpeed: 4000,
  slidesToShow: 2,
  infinite: false,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 1,
      }
    },
  ]
});