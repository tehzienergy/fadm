var gulp = require('gulp'),
    del = require('del'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    sass = require('gulp-sass'),
    pug = require('gulp-pug'),
    changed = require('gulp-changed'),
//    cache = require('gulp-cache'),
//    cached = require('gulp-cached'),
//    remember = require('gulp-remember'),
    rename = require("gulp-rename"),
    concat = require("gulp-concat"),
    insert = require('gulp-insert'),
    runSequence = require('run-sequence'),
    watch = require('gulp-watch'),
    batch = require('gulp-batch'),
    plumber = require('gulp-plumber'),
    cacheBuster = require('gulp-cachebust'),
    cachebust = new cacheBuster(),
    emitty = require('emitty').setup('src', 'pug'),
    gulpif = require('gulp-if');

gulp.task('default', function () {
    gulp.start('build');
});

gulp.task('build', [
    'fonts',
    'images',
    'scripts:jquery',
    'scripts:bootstrap',
    'scripts:plugins',
    'scripts',
    'templates',
    'styles'
]);

gulp.task('clean', function () {
  return del([
    'build/*',
  ]);
});

gulp.task('images', function() {
  gulp.src(['./src/images/*','./src/images/**/*','./src/blocks/**/images/*'])
    .pipe(plumber())
    .pipe(rename({dirname: ''}))
    .pipe(gulp.dest('./build/img'))
});

gulp.task('templates', function () {
  new Promise((resolve, reject) => {
    emitty.scan(global.emittyChangedFile).then(() => {
      gulp.src('src/pages/*.pug')
        .pipe(gulpif(global.watch, emitty.filter(global.emittyChangedFile)))
        .pipe(pug({ pretty: true }))
        .pipe(gulpif(global.isBuild,cachebust.references()))
        .pipe(gulp.dest('build'))
        .on('end', resolve)
        .on('error', reject)
        .pipe(browserSync.reload({stream:true}));
    });
  })
});

gulp.task('fonts', function () {
  gulp.src('src/fonts/**/*')
    .pipe(plumber())
    .pipe(gulp.dest('./build/fonts'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('scripts:jquery', function () {
  gulp.src('./src/scripts/jquery.min.js')
    .pipe(plumber())
    .pipe(gulp.dest('./build/js'));
});

gulp.task('scripts:bootstrap', function () {
  gulp.src('./src/scripts/bootstrap.bundle.js')
    .pipe(plumber())
    .pipe(gulp.dest('./build/js'));
});

gulp.task('scripts:plugins', function () {
  gulp.src('./src/scripts/plugins/*.js')
    .pipe(plumber())
    .pipe(concat('plugins.js'))
    .pipe(gulp.dest('./build/js'));
});

gulp.task('scripts', function () {
  gulp.src(['./src/blocks/**/*.js', './src/scripts/script.js'])
    .pipe(plumber())
    .pipe(concat('script.js'))
    .pipe(insert.wrap('$(document).ready(function(){', '})'))
    .pipe(gulp.dest('./build/js'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('server', function() {
    browserSync.init({
        server: {
            baseDir: "build"
        },
      reloadOnRestart: true,
      open: false
    });
});

gulp.task('styles', function () {
  gulp.src('./src/styles/style.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(autoprefixer({
        browsers: ['last 20 versions']
    }))
    .pipe(gulp.dest('./build/css'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('watch', function () {
    global.watch = true;
    watch(['./src/styles/*.scss', './src/styles/global/*.scss', './src/blocks/**/*.scss', './src/styles/plugins/*.scss', './src/styles/bootstrap/*.scss'], batch(function (events, done) {
        gulp.start('styles', done);
    }));
    watch(['./src/pages/*.pug','./src/templates/*.pug','./src/blocks/**/*.pug'], batch(function (events, done) {
        gulp.start('templates', done);
    }));
    gulp.watch(['./src/pages/*.pug','./src/templates/*.pug','./src/blocks/**/*.pug'], ['templates'])
    .on('all', (event, filepath) => {
      global.emittyChangedFile = filepath;
    });
    watch(['./src/images/*','./src/blocks/**/images/*'], batch(function (events, done) {
        gulp.start('images', done);
    }));
    watch(['./src/blocks/**/*.js', './src/scripts/*.js'], batch(function (events, done) {
        gulp.start('scripts', done);
    }));
    watch('./src/fonts/*', batch(function (events, done) {
        gulp.start('fonts', done);
    }));
    watch('./src/scripts/plugins/*.js', batch(function (events, done) {
        gulp.start('scripts:plugins', done);
    }));
});

gulp.task('dev', ['build', 'server', 'watch']);